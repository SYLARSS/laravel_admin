@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <form method="post" action="{{ route('articles.save') }}" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <div class="form-group">
                    <div class="form-group">
                        <label for="name">Title:</label>
                        <input type="text" class="form-control" name="title" required/>
                    </div>
                    <label for="body">Body:</label>
                    <textarea type="text" class="form-control" name="body" required></textarea>
                </div>
                <div class="form-group">
                    <label for="file">Image:</label>
                    <input type="file" name="file"
                           class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}">
                    @if ($errors->has('file'))
                        <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('file') }}</strong>

              </span>
                </div>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Create</button>
        </div>
        </form>
    </div>
    </div>
@endsection