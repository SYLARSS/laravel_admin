@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($data as $article)
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <h4><strong><a href="#">{{$article->title}}</a></strong></h4>
                            </div>
                            <div class="col-md-2 text-right">Author: {{$article->author()->first()['name']}}</div>
                            <div class="col-md-2 text-right">Created: {{$article->created_at}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">
                                    <img class="image-responsive" style="max-width: 260px" src="{{$article->image}}"
                                         alt="">
                                </a>
                            </div>
                            <div class="col-md-8">
                                <p>
                                    {{$article->body}}
                                </p>
                            </div>
                        </div>
                        @if(auth()->check() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('author')))
                            <div class="row">
                                <div class="col-md-1" style="display: inline-block">
                                    <a href="{{ route('articles.edit',$article->id)}}"
                                       class="btn btn-primary">Edit</a>
                                    <form action="{{ route('articles.delete', $article->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </div>
                            </div>

                        @endif
                    </div>
                </div>
                <hr>
                @endforeach
            </div>
@endsection