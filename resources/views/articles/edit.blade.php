@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <form method="post" action="{{ route('articles.update', $article->id) }}" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <div class="form-group">
                    <div class="form-group">
                        <label for="name">Title:</label>
                        <input type="text" class="form-control" name="title" required value="{{$article->title}}"/>
                    </div>
                    <label for="body">Body:</label>
                    <textarea type="text" class="form-control" name="body" required>{{$article->body}}</textarea>
                </div>
                <div class="form-group">
                    <label for="file">Image:</label>
                    <div class="col-md-3">
                        <img class="image-responsive" style="max-width: 260px; padding-bottom: 15px"
                             src="{{$article->image}}"
                             alt="">
                    </div>
                    <input type="file" name="file" id="image"
                           class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}"
                           value="{{$article->image}}">
                    @if ($errors->has('file'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('file') }}</strong>

                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
