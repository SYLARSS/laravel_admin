<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/roles', 'HomeController@test');
Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::get('/users/create', 'UserController@create')->name('users.create');
Route::delete('/users/{id}/delete', 'UserController@delete')->name('users.delete');
Route::post('/users', 'UserController@save')->name('users.save');
Route::post('/users/{id}', 'UserController@update')->name('users.update');
Route::get('/articles', 'ArticleController@index')->name('articles');
Route::get('/articles/create', 'ArticleController@create')->name('articles.create');
Route::post('/articles', 'ArticleController@save')->name('articles.save');
Route::get('/articles/{id}/edit', 'ArticleController@edit')->name('articles.edit');
Route::post('/articles/{id}', 'ArticleController@update')->name('articles.update');
Route::delete('/articles/{id}/delete', 'ArticleController@delete')->name('articles.delete');

