<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        return view('articles.list', ['data' => Article::orderBy('created_at', 'desc')->get()]);
    }

    public function edit(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin') && !$request->user()->hasRole('author')) {
            return redirect()->route('articles');
        }

        $article = Article::find($id);
        return view('articles.edit', compact('article'));
    }

    public function delete(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('articles');
        }
        $article = Article::find($id);
        $article->delete();
        return redirect()->route('articles');
    }

    public function create(Request $request)
    {
        if (!$request->user()->hasRole('admin') && !$request->user()->hasRole('author')) {
            return redirect()->route('articles');
        }

        return view('articles.create');
    }

    public function save(Request $request)
    {
        if (!$request->user()->hasRole('admin') && !$request->user()->hasRole('author')) {
            return redirect()->route('home');
        }

        $this->validate($request, [
            'title' => 'required|min:3|max:255',
            'body' => 'max:2555|required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('file')
                ->storeAs('uploads', $imageName, 'public');
        }

        $article = new Article();
        $article->title = $request->get('title');
        $article->body = $request->get('body');
        $article->image = '/storage/' . $path;
        $article->user_id = $request->user()->id;
        $article->save();

        return redirect()->route('articles');
    }

    public function update(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin') && !$request->user()->hasRole('author')) {
            return redirect()->route('articles');
        }

        $this->validate($request, [
            'title' => 'required|min:3|max:255',
            'body' => 'max:2555|required',
        ]);
        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('file')
                ->storeAs('uploads', $imageName, 'public');
        }

        $article = Article::find($id);
        $data = [
            'title' => $request->get('title'),
            'image' => $path ? $article->image = '/storage/' . $path : $article->image,
            'body' => $request->get('body'),
            'user_id' => $request->user()->id
        ];
        $article->update($data);
        return redirect()->route('articles');
    }
}
