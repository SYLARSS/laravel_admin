<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('articles');
        }
        return view('users.list', ['data' => User::all()]);
    }

    public function edit(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('articles');
        }
        $roles = Role::all();
        $user = User::find($id);
        return view('users.edit', ['user' => $user, 'roles' => $roles]);
    }

    public function delete(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('articles');
        }
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users');
    }

    public function create(Request $request)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('articles');
        }

        return view('users.create', ['roles' => Role::all()]);
    }

    public function save(Request $request)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('home');
        }

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users,email',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();
        if ($request->get('role') != 0) {
            $user->roles()->sync($request->get('role'));
        } else {
            $user->roles()->delete();
        }
        return redirect()->route('users');
    }

    public function update(Request $request, $id)
    {
        if (!$request->user()->hasRole('admin')) {
            return redirect()->route('home');
        }

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users,email,' . $id,
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);
        $user = User::find($id);
        $data = [
            'name' => $request->get('name'),
            'password' => Hash::make($request->get('password')),
            'email' => $request->get('email')
        ];
        if ($request->get('role') != 0) {
            $user->roles()->sync($request->get('role'));
        } else {
            $user->roles()->delete();
        }
        $user->update($data);
        return redirect()->route('users');
    }
}
