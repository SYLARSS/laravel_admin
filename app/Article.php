<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'body', 'image'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
}
