<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;
use App\User;
use Illuminate\Support\Facades\Hash;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            [
                'slug' => 'create-user',
                'name' => 'Create User Permission',
            ],
            [
                'slug' => 'edit-user',
                'name' => 'Edit User Permission',
            ],
            [
                'slug' => 'delete-user',
                'name' => 'Delete User Permission',
            ],
            [
                'slug' => 'create-article',
                'name' => 'Create Article Permission',
            ],
            [
                'slug' => 'edit-article',
                'name' => 'Edit Article Permission',
            ],
            [
                'slug' => 'delete-article',
                'name' => 'Delete Article Permission',
            ],
        ]);

        $permissions = Permission::all()->map(function ($permission) {
            return $permission->id;
        })->toArray();


        $adminRole = new Role();
        $adminRole->slug = 'admin';
        $adminRole->name = 'Admin';
        $adminRole->save();
        $adminRole->permissions()->sync($permissions);

        $authorPermissions = Permission::where('name', 'LIKE', '%Article%')->get()
            ->map(function ($permission) {
                return $permission->id;
            })->toArray();
        $authorRole = new Role();
        $authorRole->slug = 'author';
        $authorRole->name = 'Author';
        $authorRole->save();
        $authorRole->permissions()->sync($authorPermissions);

        $adminUser = new User();
        $adminUser->name = 'Admin';
        $adminUser->email = 'admin@admin.com';
        $adminUser->password = Hash::make('password');
        $adminUser->save();
        $adminUser->roles()->attach($adminRole);


    }
}
